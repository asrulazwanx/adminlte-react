import React, { Component } from 'react';
import logo from './logo.svg';
// import './App.css';
import Header from './components/Header';
import Sidebar from './components/Sidebar';
import ContentHeader from './components/ContentHeader';
import Content from './components/Content';
import Footer from './components/Footer';
import ControlSidebar from './components/ControlSidebar';

class App extends Component {
  render() {
    return (
        <div>
          <Header />
          <Sidebar />
          <ContentHeader />
          <Content />
          <Footer />
          <ControlSidebar />
        </div>
    );
  }
}

export default App;
